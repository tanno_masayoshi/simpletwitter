package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		String messageId = request.getParameter("messageId"); //formで設定したvalue値に対応するnameを代入
		if (!messageId.matches("^[0-9]*$") || StringUtils.isBlank(messageId)) {
			HttpSession session = request.getSession();
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		Message message = new MessageService().selectMessage(messageId);
		if (message == null) {
			HttpSession session = request.getSession();
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		Message message = getMessage(request); //getUserメソッド(下記)でリクエスト内のメッセージ情報をNewしたUserに引き渡し
		if (isValid(message, errorMessages)) { //isValidメソッドで入力判定を行う
			new MessageService().update(message); //MessageServiceのupdateメソッドにuser(NewしたUser)を引き渡し
		}
		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}
		response.sendRedirect("./");
	}
	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("messageId")));
		message.setText(request.getParameter("text"));

		return message;
	}
	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();
		if (StringUtils.isEmpty(text)) { //StringUtils.isEmptyでnullか空文字をtrue判定
			errorMessages.add("つぶやきを入力してください");
		}
		if (!StringUtils.isEmpty(text) && (140 < text.length())) { //StringUtils.isEmptyでnullか空文字をtrue判定
			errorMessages.add("つぶやきは140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
