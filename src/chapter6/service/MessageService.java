package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserDao;
import chapter6.dao.UserMessageDao;
import chapter6.utils.CipherUtil;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User select(String accountOrEmail, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, accountOrEmail, encPassword);

            return user;
        } catch (RuntimeException e) {
            throw e;
        } catch (Error e) {
            throw e;
        } finally {
            close(connection);
        }
    }
    public List<UserMessage> select(String userId, String sDate, String eDate) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd"); //MMは大文字
        Integer id = null;
        String startDate = null;
        String endDate = null;
        try {
        	connection = getConnection();
        	if(!StringUtils.isBlank(userId)) {
        		id = Integer.parseInt(userId); //UserMessageDaoの引数として、クリックしたユーザーIDを使用
        	}
        	Date today = new Date();
        	if(StringUtils.isBlank(sDate)) {
        		startDate = "2020-01-01 00:00:00";
        	} else {
        		startDate = sDate + " 00:00:00";
        	}
        	if(StringUtils.isBlank(eDate)) {
        		endDate = sdFormat.format(today).toString() + " 23:59:59";
        	} else {
        		endDate = eDate + " 23:59:59";
        	}

        	List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
        	return messages;
        } catch (RuntimeException e) {
        	throw e;
        } catch (Error e) {
        	throw e;
        } finally {
        	close(connection);
        }
    }
    public Message selectMessage(String messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
        	if(!StringUtils.isEmpty(messageId)) {
        		id = Integer.parseInt(messageId);
        	}
            Message message = new MessageDao().select(connection, id);

            return message;
        } catch (RuntimeException e) {
            throw e;
        } catch (Error e) {
            throw e;
        } finally {
            close(connection);
        }
    }
    public void update(Message message) {

        Connection connection = null;
        try {
            connection = getConnection(); //SQLのコネクションを作成、UserDaoに引き渡す
            new MessageDao().update(connection, message);
            commit(connection); //commitはセーブのようなもの。Daoで行った書き込みを実行する
        } catch (RuntimeException e) {
            rollback(connection); // rollbackはconnectionのUndo
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection); //connectionを閉じ、DBの編集を終了する
        }
    }
    public void deleteMessage(String messageId) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		Integer id = null;
    		if(!StringUtils.isEmpty(messageId)) {
    			id = Integer.parseInt(messageId);
    		}
    		new MessageDao().delete(connection, id);
    		commit(connection);

    		return;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
             close(connection); //コネクションもDBも開いたら閉じる!
    	}
    }
}
