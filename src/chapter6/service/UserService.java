package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

    public void insert(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User select(String accountOrEmail, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, accountOrEmail, encPassword);

            return user;
        } catch (RuntimeException e) {
            throw e;
        } catch (Error e) {
            throw e;
        } finally {
            close(connection);
        }
    }
    public User select(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, userId);

            return user;
        } catch (RuntimeException e) {
            throw e;
        } catch (Error e) {
            throw e;
        } finally {
            close(connection);
        }
    }
    public void update(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
        	if (!StringUtils.isEmpty(user.getPassword())) {
        		String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);
        	}
            connection = getConnection(); //SQLのコネクションを作成、UserDaoに引き渡す
            new UserDao().update(connection, user);
            commit(connection); //commitはセーブのようなもの。Daoで行った書き込みを実行する。select時は不要だが条件分岐時のupdate定義のため付けているときもある
        } catch (RuntimeException e) {
            rollback(connection); // rollbackはconnectionのUndo。commitが必要な処理には必ずrollback処理を含めること
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection); //connectionを閉じ、DBの編集を終了する
        }
    }
    public User select(String account) {

        Connection connection = null;
        try {
            connection = getConnection(); //DBとの現在のコネクションを取得
            User user = new UserDao().select(connection, account);
            return user;
        } catch (RuntimeException e) {
            throw e;
        } catch (Error e) {
            throw e;
        } finally {
            close(connection);
        }
    }
}