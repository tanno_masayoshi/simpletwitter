package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {
	public List<UserComment> select(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    comments.id as id, ");
			sql.append("    comments.text as text, ");
			sql.append("    comments.user_id as user_id, ");
			sql.append("    comments.message_id as message_id, ");
			sql.append("    comments.created_date as created_date, ");
			sql.append("    comments.updated_date as updated_date, ");
			sql.append("    users.id as id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");


			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery(); //executeQueryは更新の伴わない処理の実行。ここで作成されるResultSetはTable。一次配列には格納できない

			List<UserComment> comments = toComment(rs); //toMessageで複数のMessageメソッド(Tableの一行文)をListに格納する
			if (comments.isEmpty()) {
				return null;
			} else {
				return comments;
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserComment> toComment(ResultSet rs) throws SQLException {
		//上のメソッドでSelectしたMessageをBeansへ格納する手順をこちらに設けている。
		List<UserComment> comments = new ArrayList<UserComment>(); //Beansのcommentに合わせたListを生成
		try {
			while (rs.next()) {
				UserComment comment = new UserComment();
				comment.setId(rs.getInt("id"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setText(rs.getString("text"));
				comment.setCommentId(rs.getInt("message_id"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));
				comment.setUpdatedDate(rs.getTimestamp("updated_date"));
				comment.setName(rs.getString("name"));
				comment.setAccount(rs.getString("account"));

				comments.add(comment);
			}

			return comments;

		} finally {
			close(rs);
		}
	}
}
