package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, Integer id, int num, String startDate, String endDate) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    messages.id as id, "); //messagesテーブルのカラムであるidを"id"という名前で取得
            sql.append("    messages.text as text, ");
            sql.append("    messages.user_id as user_id, ");
            sql.append("    users.account as account, ");
            sql.append("    users.name as name, ");
            sql.append("    messages.created_date as created_date ");
            sql.append("FROM messages "); //FROMで指定したテーブルmessagesについて
            sql.append("INNER JOIN users "); //usersのテーブルと結合する
         	sql.append("ON messages.user_id = users.id "); //messageとusersの結合条件はidの一致
         	sql.append("WHERE messages.created_date BETWEEN ? AND ? ");
         	if (id != null) {
         		sql.append("AND users.id = ? "); //messageとusersの結合条件はidの一致
         	}
         	sql.append("ORDER BY created_date DESC limit " + num); //Daoで1000を上限に指定し、created_date順にDESC
            //StringBuilderはインスタンスの戻り値としてテーブルを返す

            ps = connection.prepareStatement(sql.toString()); //prepareStatement使用のためSQL文をStringにしてpsに格納
            ps.setString(1, startDate);
            ps.setString(2, endDate);
            if (id != null) {
            	ps.setInt(3, id);
            }
            ResultSet rs = ps.executeQuery(); // PreparedStatementのSQLクエリーを実行し、生成されたResultSetオブジェクトをrsとする

            List<UserMessage> messages = toUserMessages(rs); //このメソッドだけではMessageを取得しただけで終わる。toUserMessageでBeansへ格納
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {
    	//上のメソッドでSelectしたUserMessageをBeansへ格納する手順をこちらに設けている。
        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setAccount(rs.getString("account"));
                message.setName(rs.getString("name"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}