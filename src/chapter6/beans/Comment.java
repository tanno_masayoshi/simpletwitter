package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable{
	//DaoでSelectしたデータを保存するには、List<Beans> name = new ArrayList<Beans>() によってList化する
	//Tableは一行とは限らないため、各要素の定義を含んだBeansを配列化してTableを格納できるようにする。
	private int id;
	private String text;
	private int userId;
	private int commentId;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
