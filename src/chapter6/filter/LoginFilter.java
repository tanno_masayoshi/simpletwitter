package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

//FilterはServletが起動する前に起動することができる。
@WebFilter(urlPatterns={"/setting", "/edit"})
public class LoginFilter implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		//このメソッドのchain.doFilter(request,response)が処理された後、各Servletの処理が発生する。
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		HttpSession session = httpRequest.getSession();
		User user = (User) session.getAttribute("loginUser");

		if (user == null) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);
            httpResponse.sendRedirect("./login");
            return;

		} else {
			chain.doFilter(httpRequest, httpResponse); //requestとresponseをservletに引き渡す。
			//この場合は、ifが不適合ならresponseとrequestをそのまま(型変換して)引き渡す
		}
	}
	@Override
	public void init(FilterConfig config) {
	}
	//アプリケーション起動時にコールされ,パラメータの初期化などを行う
	//initはFilterクラスに定義されており、中身が存在しなくても必ず設置

	@Override
	public void destroy() {
	}
	//アプリケーション停止時にコールされ、保持しているインスタンスの破棄などを行う。
	//destroyはFilterクラスに定義されており、中身が存在しなくても必ず設置
}