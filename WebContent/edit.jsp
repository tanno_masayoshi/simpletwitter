<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> <!-- pageで使用する言語の指定 -->
<%@page isELIgnored="false"%> <!-- true：EL式を使用しない、false：EL式を使用する -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <!-- タグライブラリの指定。cタグを使用するときURLとともに指定 -->
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> <!-- タグライブラリの指定。fmtタグを使用するときURLとともに指定 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> <!-- ドキュメントタイプとHTMLの指定とサーバの指定 -->
<html>
    <head>
        <link href="./css/style.css" rel="stylesheet" type="text/css"> <!-- スタイルシートの適用 -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <!-- equivは文書の処理の方法、contentは以下のコードのメタ指定 -->
        <script type="text/javascript" src="./js/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="./js/my-jquery.js"></script>
        <title>つぶやきの編集</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }"> <!-- messsageの文字が空白かnullの場合にServletからエラーを返す -->
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
            <div class="profile">
                <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
                <div class="account">@<c:out value="${loginUser.account}" /></div>
                <div class="description"><c:out value="${loginUser.description}" /></div>
            </div>
            <form action="edit" method="post"><br /> <!-- formタグのactionはrequestの送信先 -->
                <input name="messageId" value="${message.id}" id="messageId" type="hidden">
                    <textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${message.text}"/></textarea>
                    <br />
                <input type="submit" value="更新" id="edit"/> <br />
            </form>
            <a href="./">戻る</a>

            <div class="copyright"> Copyright(c)Tanno Masayoshi</div>
        </div>
    </body>
</html>