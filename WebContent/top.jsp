<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><!-- pageで使用する言語の指定 -->
<%@page isELIgnored="false"%> <!-- true：EL式を使用しない、false：EL式を使用する -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <!-- タグライブラリの指定。cタグを使用するときURLとともに指定 -->
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> <!-- タグライブラリの指定。fmtタグを使用するときURLとともに指定 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> <!-- ドキュメントタイプとHTMLの指定とサーバの指定 -->
<html>
    <head>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="./js/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="./js/my-jquery.js"></script>
        <title>簡易Twitter</title>
    </head>
    <body>
	    <div class="main-contents">
		    <div class="header">
			    <c:if test="${ empty loginUser }">
				    <a href="login">ログイン</a>
				    <a href="signup">登録する</a>
			    </c:if>
			    <c:if test="${ not empty loginUser }">
				    <a href="./">ホーム</a> <!--同一ディレクトリ内を実行(この場合は再読込)-->
			    	<a href="setting">設定</a> <!--setting.jspに紐付いたSettingServletへ連携-->
				    <a href="logout">ログアウト</a>
			    </c:if>
		    </div>
		    <c:if test="${ not empty loginUser }">
			    <div class="profile">
				    <div class="name">
					    <h2><c:out value="${loginUser.name}" /></h2>
				    </div>
				    <div class="account">@<c:out value="${loginUser.account}" />
				    </div>
				    <div class="description">
					    <c:out value="${loginUser.description}" />
				    </div>
			    </div>
		    </c:if>
		    <div class="errorMessages">
		        <c:if test="${ not empty errorMessages }">
				    <ul>
					    <c:forEach items="${errorMessages}" var="errorMessage">
						    <li><c:out value="${errorMessage}" />
					    </c:forEach>
				    </ul>
				</c:if>
			    <c:remove var="errorMessages" scope="session" />
		    </div>
		    <div class="form-area">
			    <c:if test="${ isShowMessageForm }"><!-- TopServletでユーザーのログインを確認した場合のみ表示 -->
				    <div class="narrowing-area">
				        <div style="display: inline-flex">
		                    <form action="./">日付
		                            <input type="date" name="startDate" value="${startDate}"/>～<input type="date" name="endDate" value="${endDate}"/>
		                            <input type="submit" value="絞り込み"/>
		                    </form>
		                </div>
		            </div>
		            <div class="text">
				        <form action="message" method="post">いま、どうしてる？<br />
					        <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					        <br /> <input type="submit" value="つぶやく">（140文字まで）
				        </form>
				    </div>
			    </c:if>
		    </div><!-- forEachでは%(Java表記)を用いてitemに入れるリストを定義、または以下のようにtop.jsp呼び出し時に${Message}をsetして解決する -->
		    <div class="messages"><!-- messageクラス。ユーザーのつぶやきを表示 -->
			    <c:forEach items="${messages}" var="message"><!-- messagesにはmessageが取り扱う各要素がまとめて格納。varはカウント変数名 -->
				    <div class="message">
					    <div class="account-name">
						    <span class="account">
						        <a href="./?user_id=<c:out value="${message.userId}"/>"> <!-- ここをクリックした時のみ/?user_idを返す -->
								    <c:out value="${message.account}" />
						        </a>
						    </span>
						    <span class="name"><c:out value="${message.name}" /></span>
					    </div>
					    <div class="text"><c:out value="${message.text}" />
					    </div>
					    <div class="date">
						    <fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					    </div>
					    <c:if test="${loginUser.id == message.userId}">
					        <div class="button">
						    <div style="display: inline-flex">
							    <form action="edit">
									    <input type="hidden" name="messageId" value="${message.id}">
									    <!-- idはCSSやServlet、JavaScriptでタグを指定する際に使われる。nameと近いが文字参照不可、タグごとに一つのみ設定可能 -->
									    <input type="submit" value="編集" ><!-- nameつけなければurlに表示されない -->
							    </form>
							    <form action="deleteMessage" method="post">
								    <input type="hidden" name="messageId" value="${message.id}">
								    <input type="submit" value="削除" id="delete">
							    </form>
						    </div>
						    </div>
					    </c:if>
					    <c:if test="${ isShowMessageForm }"><!-- TopServletでユーザーのログインを確認した場合のみ表示 -->
						    <form action="comment" method="post">
						        <input type="hidden" name="messageId" value="${message.id}">
							    <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
							    <br />
							    <input type="submit" value="返信">（140文字まで）
						    </form>
					    </c:if>
					    <c:forEach items="${comments}" var="comment">
					        <div class="comment">
					            <c:if test="${ comment.commentId == message.id }">
						            <div class="account-name">
							            <span class="account">
							                <c:out value="${comment.account}" />
							            </span>
							            <span class="name">
						                    <c:out value="${comment.name}" />
							            </span>
							            <div class="text"><c:out value="${comment.text}" />
				                        </div>
				                        <div class="date">
						                    <fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
					                    </div>
						            </div>
						        </c:if>
					        </div>
					    </c:forEach>
				    </div>
			    </c:forEach>
		    </div>
		    <div class="copyright">Copyright(c)Tanno Masayoshi</div>
	    </div>
    </body>
</html>