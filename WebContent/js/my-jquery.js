//jQueryは各HTMLのbody部分に書き込みか、またはこのように別途ファイルへ書き込んだ後読み込みを行う。読み込みのための記述箇所はjQueryライブラリのあと
$(function() { //jQueryはHTMLがすべて読み込まれる前に作動するとエラーが生じる。そのためfunction(.ready)をセットして、読み込み完了後に内部要素を作動させる
  $('#delete').click(function(){
	    if(!confirm('つぶやきを削除しますか？')){
	        //cancel時の処理 */
	        return false;
	    }else{
	        //ok時の処理 */
	        location.href = './';
	    }
	});


  $('#edit').on('click', function() {
    alert('つぶやきの内容を更新しました');
  });
});
